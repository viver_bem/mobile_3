import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { TextInput, StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { useNavigation } from '@react-navigation/native'; 
import logoBV from '../assets/logoBV.png'; 
import api from './axios/axios'; 
import Animated, { SlideInUp, SlideOutDown } from 'react-native-reanimated';

export default function Cadastro() {
  const navigation = useNavigation(); 
  const [email, setEmail] = useState(''); 
  const [name, setName] = useState(''); 
  const [apartment, setApartment] = useState(''); 
  const [block, setBlock] = useState(''); 
  const [password, setPassword] = useState(''); 
  const [telephone, setTelephone] = useState(''); 
  const [alertVisible, setAlertVisible] = useState(false); 
  const [alertMessage, setAlertMessage] = useState(''); 

  const handleRegister = async () => {
    // Verificação se todos os campos obrigatórios foram preenchidos
    if (!email || !name || !apartment || !block || !password || !telephone) {
      setAlertMessage("Preencha todos os campos obrigatórios.");
      setAlertVisible(true);
      setTimeout(() => setAlertVisible(false), 3000); 
      return;
    } 

    try {
      // Chamada para a API de cadastro de usuário
      const response = await api.postUser({
        email,
        name,
        apartment,
        block,
        password,
        telephone
      });

      if (response.status === 201) {
        // Sucesso no cadastro
        setAlertMessage("Usuário criado com sucesso");
        setAlertVisible(true);
        setTimeout(() => {
          setAlertVisible(false);
          navigation.navigate('Login'); 
        }, 3000);
      } else {
        // Erro no cadastro
        setAlertMessage(response.data.error || "Erro ao cadastrar");
        setAlertVisible(true);
        setTimeout(() => setAlertVisible(false), 3000);
      }
    } catch (error) {
      // Tratamento de erro no cadastro
      let message = "Erro desconhecido. Por favor, tente novamente.";
      if (error.response) {
        message = error.response.data.error || "Erro ao cadastrar. Por favor, tente novamente.";
      } else if (error.request) {
        message = "Erro de conexão. Verifique sua rede e tente novamente.";
      }
      setAlertMessage(message);
      setAlertVisible(true);
      setTimeout(() => setAlertVisible(false), 3000);
    }
  };

  // Função para filtrar caracteres não numéricos nos campos de apartamento, bloco e telefone
  const handleNumericInput = (setFunction, value) => {
    const numericValue = value.replace(/[^0-9]/g, '');
    setFunction(numericValue);
  };

  return (
    <View style={styles.container}>
      <Image source={logoBV} style={styles.image} /> 
      <View style={styles.inputsContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
        />
        <TextInput
          style={styles.input}
          placeholder="Nome"
          onChangeText={setName}
          value={name}
        />
        <TextInput
          style={styles.input}
          placeholder="Apartamento"
          keyboardType="numeric"
          onChangeText={(value) => handleNumericInput(setApartment, value)}
          value={apartment}
        />
        <TextInput
          style={styles.input}
          placeholder="Bloco"
          keyboardType="numeric"
          onChangeText={(value) => handleNumericInput(setBlock, value)}
          value={block}
        />
        <TextInput
          style={styles.input}
          placeholder="Telefone"
          keyboardType="numeric"
          onChangeText={(value) => handleNumericInput(setTelephone, value)}
          value={telephone}
        />
        <TextInput
          style={styles.input}
          placeholder="Senha"
          secureTextEntry={true}
          onChangeText={setPassword}
          value={password}
        />
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={handleRegister}>
          <Text style={styles.buttonText}>Cadastre-se</Text>
        </TouchableOpacity>
      </View>
      <StatusBar style="auto" /> 
      {alertVisible && (
        <Animated.View
          entering={SlideInUp.duration(500)}
          exiting={SlideOutDown.duration(500)}
          style={styles.alertContainer}
        >
          <Text style={styles.alertText}>{alertMessage}</Text>
        </Animated.View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#463e31", 
  },
  inputsContainer: {
    width: "80%",
    marginBottom: 20,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingHorizontal: 10,
    backgroundColor: "#E7DDDA",
    marginBottom: 10,
    opacity: 0.8,
  },
  buttonContainer: {
    width: "60%",
    marginBottom: 100,
    marginTop: 20,
  },
  button: {
    backgroundColor: "#E7DDDA",
    borderRadius: 10,
    padding: 10,
    marginBottom: 4,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: "black",
    fontWeight: "bold",
  },
  image: {
    height: "40%",
    width: "100%",
    marginBottom: 40,
  },
  alertContainer: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
    padding: 10,
    backgroundColor: '#333',
    borderRadius: 10,
    alignItems: 'center',
  },
  alertText: {
    color: 'white',
    fontSize: 16,
  },
});
