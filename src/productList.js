import React, { useState, useEffect, useLayoutEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  Image,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";
import iconUser from "../assets/iconUser.png"; 

const ProductList = ({ route }) => {
  const { user } = route.params;
  const navigation = useNavigation();
  const [areas, setAreas] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getArea = async () => {
      try {
        const response = await api.getAreas();
        if (response.status !== 200) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        setAreas(response.data);
      } catch (error) {
        console.error("Erro ao buscar as áreas:", error);
      } finally {
        setLoading(false);
      }
    };
    getArea();
  }, []);

  // Função para lidar com a ação de pressionar o botão de perfil do usuário
  const handleUserOk = (user) => {
    navigation.navigate("PerfilUser", { user });
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={() => handleUserOk(user)} style={styles.headerButton}>
          <Image source={iconUser} style={styles.headerImage} />
        </TouchableOpacity>
      ),
    });
  }, [navigation, user]);

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#ECEBE9" />
        <Text style={styles.loadingText}>Carregando áreas...</Text>
      </View>
    );
  }

  const handlePress = (area) => {
    navigation.navigate("BookingDefault", { area, user });
  };

  const renderItem = ({ item: area }) => (
    <View key={area.ID_area} style={styles.productContainer}>
      <View style={{ flexDirection: "row" }}>
        <View style={styles.descriptionContainer}>
          <TouchableOpacity style={styles.button} onPress={() => handlePress(area)}>
            <Text style={styles.name}>{area.Name}</Text>
            <Text style={styles.description}>Capacidade: {area.Capacity}</Text>
            <Text style={styles.description}>Normas: {area.Norms}</Text>
            <Text style={styles.buttonText}>Verificar Horários</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      {areas.length === 0 ? (
        <Text style={styles.errorText}>Nenhuma área disponível</Text>
      ) : (
        <FlatList
          data={areas}
          renderItem={renderItem}
          keyExtractor={(item) => item.ID_area.toString()}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#463e31",
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  loadingText: {
    marginTop: 10,
    color: "#ECEBE9",
    fontSize: 18,
  },
  errorText: {
    color: "#ECEBE9",
    fontSize: 18,
    textAlign: "center",
    marginTop: 20,
  },
  productContainer: {
    marginTop: 40,
  },
  descriptionContainer: {
    marginLeft: 16,
    flex: 1,
  },
  description: {
    fontSize: 16,
    marginBottom: 8,
    color: "#392620",
    textAlign: "left",
  },
  name: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 4,
    color: "#392620",
  },
  button: {
    backgroundColor: "#E7DDDA",
    borderRadius: 10,
    padding: 15,
    marginBottom: 4,
    maxWidth: 4000,
    marginTop: 20,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: "#001",
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  headerButton: {
    marginRight: 10,
  },
  headerImage: {
    width: 30,
    height: 30,
  },
});

export default ProductList;
