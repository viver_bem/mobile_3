import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { TextInput, StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import logoBV from '../assets/logoBV.png';
import { useNavigation } from '@react-navigation/native';

export default function PageInit() {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Image source={logoBV} style={styles.imagem} />

      <Text style={styles.bvindo}>Bem-vindo ao nosso aplicativo de locações de espaço gourmet!</Text>

      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Login')}>
          <Text style={styles.buttonText}>Entrar</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Cadastro')}>
          <Text style={styles.buttonText}>Cadastrar</Text>
        </TouchableOpacity>
      </View>

      <StatusBar style="default" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#463e31',
  },
  buttonContainer: {
    width: '80%',
  },
  button: {
    backgroundColor: '#E7DDDA',
    borderRadius: 10,
    padding: 15,
    marginBottom: 10,
    alignItems: 'center',
    borderColor: 'black',
    borderWidth: 1,
  },
  buttonText: {
    color: 'black',
    fontWeight: 'bold',
  },
  imagem: {
    height: '40%',
    width: '100%',
    marginTop: -50,
    marginBottom: 40,
  },
  bvindo: {
    color: '#ECEBE9',
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: 20,
    lineHeight: 24,
    marginBottom: 40,
  },
});
