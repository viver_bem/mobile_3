import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  TextInput,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Modal,
} from "react-native";
import logoBV from "../assets/logoBV.png"; 
import { useNavigation } from "@react-navigation/native"; 
import { Feather } from "@expo/vector-icons"; 
import api from "./axios/axios"; 
import AsyncStorage from "@react-native-async-storage/async-storage"; 
import Animated, { SlideInUp, SlideOutDown } from 'react-native-reanimated';

export default function Login() {
  const navigation = useNavigation(); 
  const [email, setEmail] = useState(""); 
  const [password, setPassword] = useState(""); 
  const [showPassword, setShowPassword] = useState(false); 
  const [eyeIcon, setEyeIcon] = useState("eye"); 
  const [alertVisible, setAlertVisible] = useState(false); 
  const [alertMessage, setAlertMessage] = useState(''); 

  const handleLogin = async () => {
    // Verificação se email e senha foram preenchidos
    if (!email || !password) {
      setAlertMessage("Email e senha são obrigatórios");
      setAlertVisible(true);
      setTimeout(() => setAlertVisible(false), 3000); 
      return;
    }

    try {
      // Chamada para a API de login
      const response = await api.Login({
        params: { email, password },
      });

      if (response.status === 200) {
        // Sucesso no login
        setAlertMessage(response.data.message);
        setAlertVisible(true);
        setTimeout(() => {
          setAlertVisible(false);
          navigation.navigate("ProductList", { user: response.data.user }); // Navegar para ProductList com os dados do usuário
        }, 3000);
        setEmail("");
        setPassword("");

        const userData = response.data;
        await AsyncStorage.setItem("userData", JSON.stringify(userData)); // Armazenar dados do usuário localmente
      } else {
        // Erro no login
        setAlertMessage(response.data.error || "Ocorreu um erro ao entrar");
        setAlertVisible(true);
        setTimeout(() => setAlertVisible(false), 3000);
      }
    } catch (error) {
      // Tratamento de erro no login
      let message = "Verifique suas informações";
      if (error.response && error.response.data) {
        message = error.response.data.error || "Ocorreu um erro ao entrar";
      }
      setAlertMessage(message);
      setAlertVisible(true);
      setTimeout(() => setAlertVisible(false), 3000);
    }
  };

  const mostrarSenha = () => {
    // Alternar visibilidade da senha e ícone do olho
    setShowPassword(!showPassword);
    setEyeIcon(showPassword ? "eye" : "eye-off");
  };

  return (
    <View style={styles.container}>
      <Image source={logoBV} style={styles.imagem} />
      <View style={styles.inputsContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
          autoCapitalize="none"
          keyboardType="email-address"
        />
        <View style={styles.passwordInputContainer}>
          <TextInput
            style={styles.passwordInput}
            placeholder="Senha"
            secureTextEntry={!showPassword}
            onChangeText={setPassword}
            value={password}
            onFocus={() => setEyeIcon("eye")}
          />
          {password !== "" && (
            <TouchableOpacity onPress={mostrarSenha} style={styles.eyeIcon}>
              <Feather name={eyeIcon} size={24} color="black" />
            </TouchableOpacity>
          )}
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={handleLogin}>
          <Text style={styles.buttonText}>Entrar</Text>
        </TouchableOpacity>
      </View>
      <StatusBar style="auto" />
      {alertVisible && (
        <Animated.View
          entering={SlideInUp.duration(500)}
          exiting={SlideOutDown.duration(500)}
          style={styles.alertContainer}
        >
          <Text style={styles.alertText}>{alertMessage}</Text>
        </Animated.View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#463e31",
  },
  inputsContainer: {
    width: "80%",
    marginBottom: 5,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingHorizontal: 10,
    backgroundColor: "#E7DDDA",
    marginBottom: 5,
    opacity: 0.8,
  },
  passwordInputContainer: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  passwordInput: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingLeft: 10,
    backgroundColor: "#E7DDDA",
    marginBottom: 5,
    opacity: 0.8,
  },
  eyeIcon: {
    position: "absolute",
    right: 10,
    top: 10,
  },
  buttonContainer: {
    width: "60%",
    marginBottom: 100,
    marginTop: 20,
  },
  button: {
    backgroundColor: "#E7DDDA",
    borderRadius: 10,
    padding: 10,
    marginBottom: 4,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: "black",
    fontWeight: "bold",
  },
  imagem: {
    height: "40%",
    width: "100%",
    marginTop: -50,
    marginBottom: 40,
  },
  alertContainer: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
    padding: 10,
    backgroundColor: '#333',
    borderRadius: 10,
    alignItems: 'center',
  },
  alertText: {
    color: 'white',
    fontSize: 16,
  },
});
