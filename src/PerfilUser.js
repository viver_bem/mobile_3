import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Modal, StyleSheet, TextInput, FlatList } from 'react-native';
import api from './axios/axios';
import { format, parseISO } from 'date-fns';
import { ptBR } from 'date-fns/locale';

const PerfilUser = ({ route }) => {
  const { user } = route.params;
  const [loading, setLoading] = useState(true);
  const [bookings, setBookings] = useState([]);
  const [areas, setAreas] = useState([]);
  const [selectedBooking, setSelectedBooking] = useState(null);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);
  const [editUserInfoModalVisible, setEditUserInfoModalVisible] = useState(false);
  const [alertVisible, setAlertVisible] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [date, setDate] = useState('');
  const [newName, setNewName] = useState('');
  const [newApartment, setNewApartment] = useState('');
  const [newBlock, setNewBlock] = useState('');
  const [newTelephone, setNewTelephone] = useState('');
  const [newPassword, setNewPassword] = useState('');

  // Buscar dados de reservas e áreas
  useEffect(() => {
    const getBookingsByEmail = async (email) => {
      try {
        const response = await api.getBooking(email);
        if (response.status !== 200) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        setBookings(response.data);
      } catch (error) {
        console.error("Erro ao buscar as reservas:", error);
      } finally {
        setLoading(false);
      }
    };

    const getArea = async () => {
      try {
        const response = await api.getAreas();
        if (response.status !== 200) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        setAreas(response.data);
      } catch (error) {
        console.error("Erro ao buscar as áreas:", error);
      }
    };

    getBookingsByEmail(user.Email);
    getArea();
  }, [user.Email]);

  // Função para editar uma reserva
  const handleEdit = (booking) => {
    setSelectedBooking(booking);
    setDate(format(parseISO(booking.Date_init), 'yyyy-MM-dd'));
    setEditModalVisible(true);
  };

  // Função para deletar uma reserva  
  const handleDelete = (booking) => {
    setSelectedBooking(booking);
    setDeleteModalVisible(true);
  };

  // Função para editar as informações do usuário
  const handleEditUserInfo = () => {
    setEditUserInfoModalVisible(true);
    // Preencher os campos de edição com as informações atuais do usuário
    setNewName(user.Name);
    setNewApartment(user.Apartment);
    setNewBlock(user.Block);
    setNewTelephone(user.Telephone);
  };

  // Função para confirmar a edição de uma reserva
  const confirmEdit = async () => {
    try {
      const response = await api.putBooking({
        ID_booking: selectedBooking.ID_Booking,
        Date_init: date,
        Date_end: date
      });
      if (response.status === 200) {
        const updatedBookings = bookings.map((booking) =>
          booking.ID_Booking === selectedBooking.ID_Booking
            ? { ...booking, Date_init: date, Date_end: date }
            : booking
        );
        setBookings(updatedBookings);
        setEditModalVisible(false);
        setSelectedBooking(null);

        setAlertMessage('Reserva atualizada com sucesso!');
        setAlertVisible(true);

        setTimeout(() => {
          setAlertVisible(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Erro ao atualizar a reserva:", error);
    }
  };

  // Função para confirmar a exclusão de uma reserva
  const confirmDelete = async () => {
    try {
      const response = await api.deleteBooking(selectedBooking.ID_Booking);
      if (response.status === 200) {
        const updatedBookings = bookings.filter((booking) => booking.ID_Booking !== selectedBooking.ID_Booking);
        setBookings(updatedBookings);
        setDeleteModalVisible(false);
        setSelectedBooking(null);

        setAlertMessage('Reserva deletada com sucesso!');
        setAlertVisible(true);

        setTimeout(() => {
          setAlertVisible(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Erro ao deletar a reserva:", error);
    }
  };

  // Função para atualizar as informações do usuário
  const handleUserInfoUpdate = async () => {
    try {
      const response = await api.putUser({
        name: newName,
        apartment: newApartment,
        block: newBlock,
        telephone: newTelephone,
        email: user.Email, // Mantém o mesmo email do usuário atual
        password: newPassword // Nova senha, se aplicável
      });
      if (response.status === 200) {
        // Atualiza as informações do usuário localmente
        user.Name = newName;
        user.Apartment = newApartment;
        user.Block = newBlock;
        user.Telephone = newTelephone;
        setEditUserInfoModalVisible(false);
        setAlertMessage('Informações atualizadas com sucesso!');
        setAlertVisible(true);
        setTimeout(() => {
          setAlertVisible(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Erro ao atualizar as informações do usuário:", error);
    }
  };

  // Função para formatar a data para o padrão brasileiro
  const formatDate = (dateString) => {
    try {
      return format(parseISO(dateString), 'dd/MM/yyyy', { locale: ptBR });
    } catch (error) {
      console.error("Erro ao formatar a data:", error);
      return dateString;
    }
  };

  // Função para renderizar cada reserva
  const renderBooking = ({ item }) => {
    return (
      <View key={item.ID_Booking} style={styles.productContainer}>
        <View style={styles.descriptionContainer}>
          <Text style={styles.descriptionModal}>Data: {formatDate(item.Date_init)}</Text>
          <Text style={styles.descriptionModal}>Área: {item.FK_ID_Area}</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.editButton} onPress={() => handleEdit(item)}>
            <Text style={styles.buttonText}>Editar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.deleteButton} onPress={() => handleDelete(item)}>
            <Text style={styles.buttonText}>Deletar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.name}>Nome do Usuário:</Text>
      <Text style={styles.description}>{user.Name}</Text>
      <Text style={styles.name}>Email:</Text>
      <Text style={styles.description}>{user.Email}</Text>
      <Text style={styles.name}>Apartamento:</Text>
      <Text style={styles.description}>{user.Apartment}</Text>
      <Text style={styles.name}>Bloco:</Text>
      <Text style={styles.description}>{user.Block}</Text>
      <Text style={styles.name}>Reservas:</Text>

      {loading ? (
        <View style={styles.loadingContainer}>
          <Text style={styles.loadingText}>Carregando...</Text>
        </View>
      ) : bookings.length === 0 ? (
        <Text style={styles.errorText}>Nenhuma reserva criada</Text>
      ) : (
        <FlatList
          data={bookings}
          renderItem={renderBooking}
          keyExtractor={item => item.ID_Booking.toString()}
        />
      )}

      {/* Botão de atualização de informações */}
      <TouchableOpacity style={styles.updateButton} onPress={handleEditUserInfo}>
        <Text style={styles.buttonText}>Atualizar Informações</Text>
      </TouchableOpacity>

      {/* Modal de edição de reserva */}
      <Modal animationType="fade" transparent={true} visible={editModalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalText}>Editar reserva para {selectedBooking ? formatDate(selectedBooking.Date_init) : ''}?</Text>
            <TextInput
              style={styles.input}
              value={date}
              onChangeText={setDate}
              placeholder="YYYY-MM-DD"
            />
            <View style={styles.modalButtonContainer}>
              <TouchableOpacity style={[styles.modalButton, styles.confirmButton]} onPress={confirmEdit}>
                <Text style={styles.modalButtonText}>CONFIRMAR</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.modalButton, styles.cancelButton]} onPress={() => setEditModalVisible(false)}>
                <Text style={styles.modalButtonText}>CANCELAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      {/* Modal de exclusão de reserva */}
      <Modal animationType="fade" transparent={true} visible={deleteModalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalText}>Você deseja deletar a reserva do dia {selectedBooking ? formatDate(selectedBooking.Date_init) : ''}?</Text>
            <View style={styles.modalButtonContainer}>
              <TouchableOpacity style={[styles.modalButton, styles.confirmButton]} onPress={confirmDelete}>
                <Text style={styles.modalButtonText}>CONFIRMAR</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.modalButton, styles.cancelButton]} onPress={() => setDeleteModalVisible(false)}>
                <Text style={styles.modalButtonText}>CANCELAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      {/* Modal de edição de informações do usuário */}
      <Modal animationType="fade" transparent={true} visible={editUserInfoModalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalText}>Editar informações do usuário</Text>
            <TextInput
              style={styles.input}
              value={newName}
              onChangeText={setNewName}
              placeholder="Novo Nome"
            />
            <TextInput
              style={styles.input}
              value={newApartment}
              onChangeText={setNewApartment}
              placeholder="Novo Apartamento"
            />
            <TextInput
              style={styles.input}
              value={newBlock}
              onChangeText={setNewBlock}
              placeholder="Novo Bloco"
            />
            <TextInput
              style={styles.input}
              value={newTelephone}
              onChangeText={setNewTelephone}
              placeholder="Novo Telefone"
            />
            <TextInput
              style={styles.input}
              value={newPassword}
              onChangeText={setNewPassword}
              placeholder="Nova Senha"
              secureTextEntry={true}
            />
            <View style={styles.modalButtonContainer}>
              <TouchableOpacity style={[styles.modalButton, styles.confirmButton]} onPress={handleUserInfoUpdate}>
                <Text style={styles.modalButtonText}>CONFIRMAR</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.modalButton, styles.cancelButton]} onPress={() => setEditUserInfoModalVisible(false)}>
                <Text style={styles.modalButtonText}>CANCELAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      {/* Exibição de alertas */}
      {alertVisible && (
        <View style={styles.alertContainer}>
          <Text style={styles.alertText}>{alertMessage}</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#463e31',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingText: {
    marginTop: 10,
    color: '#ECEBE9',
    fontSize: 18,
  },
  errorText: {
    color: '#ECEBE9',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 20,
  },
  productContainer: {
    marginTop: 40,
    backgroundColor: '#FFF',
    padding: 16,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  descriptionContainer: {
    marginBottom: 16,
  },
  description: {
    fontSize: 18,
    marginBottom: 8,
    color: "#DCDCDC",
    textAlign: "left",
  },
  descriptionModal: {
    fontSize: 18,
    marginBottom: 8,
    color: "#363636",
    textAlign: "left",
  },
  name: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 4,
    color: "#fff",
  },
  buttonContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  editButton: {
    backgroundColor: '#E7DDDA',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
    width: '100%',
  },
  deleteButton: {
    backgroundColor: '#E7DDDA',
    padding: 10,
    borderRadius: 5,
    width: '100%',
  },
  buttonText: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    width: '80%',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  modalText: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  modalButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalButton: {
    padding: 12,
    borderRadius: 5,
    marginHorizontal: 10,
  },
  confirmButton: {
    backgroundColor: '#4CAF50',
  },
  cancelButton: {
    backgroundColor: '#f44336',
  },
  modalButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  input: {
    width: '100%',
    padding: 10,
    marginVertical: 10,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
  },
  alertContainer: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
    padding: 10,
    backgroundColor: '#333',
    borderRadius: 10,
    alignItems: 'center',
  },
  alertText: {
    color: 'white',
    fontSize: 16,
  },
  updateButton: {
    backgroundColor: '#E7DDDA',
    padding: 12,
    borderRadius: 5,
    marginTop: 20,
  },
});

export default PerfilUser;
