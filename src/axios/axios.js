import axios from 'axios';
const api = axios.create({
    baseURL: "http://192.168.56.1:5000/sistema/",//Mudar a rota sempre que iniciar o projeto
    headers: {
        'accept': 'application/json',
    },
});


// Objeto com métodos para interagir com a API
const sheets = {

    // Métodos para manipulação de usuários
    postUser: (user) => api.post("cadastro/", user), // Cria um novo usuário
    getUsers: () => api.get("getuser/"), // Puxa todos os usuários
    putUser: (user) => api.put("updateuser/", user), // Atualiza um usuário existente
    deleteUser: (_id) => api.delete("deleteuser/" + _id), // Deleta um usuário existente

    // Métodos para autenticação de usuários
    Login: (user) => api.get("login/", user), // Realiza o login do usuário
    getLogin: (user) => api.get("getlogin/", user), // Puxa informações do usuário logado

    // Métodos para manipulação de tabelas
    getTables: () => api.get("getTables/"), // Puxa todas as tabelas
    getTablesDesc: () => api.get("getDescTable/"), // Puxa descrição de todas as tabelas
    
    // Métodos para manipulação de áreas
    postArea: (area) => api.post("areapost/", area), // Cria uma nova área
    getAreas: () => api.get("areaget/"), // Puxa todas as áreas
    getAreaByName: (_id) => api.get("areagetName/"), // Puxa uma área pelo nome
    updateArea: (_id) => api.put("areaput/" + _id), // Atualiza uma área existente
    deleteArea: (_id) => api.delete("areadel/" + _id), // Deleta uma área existente

    // Métodos para manipulação de reservas
    createBooking: (booking) => api.post("booking/", booking), // Cria uma nova reserva
    getAllBookings: (booking) => api.get("getAllBookings/", booking), // Puxa todas as reservas
    getBooking: (email) => api.get(`/getBookingEmail/${email}`), // Puxa uma reserva pelo email
    putBooking: (booking) => api.put("bookingput/", booking), // Atualiza uma reserva existente
    deleteBooking: (ID_booking) => api.delete(`/booking/${ID_booking}`) // Deleta uma reserva existente
}

export default sheets;