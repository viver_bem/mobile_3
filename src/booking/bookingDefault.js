import React, { useState, useLayoutEffect } from 'react';
import { View, Text, TouchableOpacity, Modal, StyleSheet, FlatList, StatusBar,  Image} from 'react-native';
import { format, addDays, startOfWeek, isAfter, parse } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import Animated, { SlideInUp, SlideOutDown } from 'react-native-reanimated';
import iconUser from "../../assets/iconUser.png";
import api from '../axios/axios';

// Componente principal de reserva e consts  
const BookingDefault = ({ navigation, route }) => {
  const { area, user } = route.params;
  const [selectedDay, setSelectedDay] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [alertVisible, setAlertVisible] = useState(false);
  const [successAlertVisible, setSuccessAlertVisible] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  const startOfWeekDate = startOfWeek(new Date(), { weekStartsOn: 0 });
  const today = new Date();

  
  // Gera os próximos 15 dias a partir do início da semana, filtrando apenas os dias atuais e futuros
  const weekDays = Array.from({ length: 15 }, (_, index) => {
    const currentDate = addDays(startOfWeekDate, index);
    const formattedDate = format(currentDate, 'dd/MM/yyyy', { locale: ptBR });
    return formattedDate; // Retorna a data formatada
  }).filter(dateString => {
    const date = parse(dateString, 'dd/MM/yyyy', new Date()); // Converte a string de data para objeto Date
    return isAfter(date, today) || format(date, 'dd/MM/yyyy') === format(today, 'dd/MM/yyyy'); // Filtra datas passadas
  });

    // Função para lidar com a ação de pressionar o botão de perfil do usuário
    const handleUserOk = (user) => {
      navigation.navigate("PerfilUser", { user });
    };
  
    useLayoutEffect(() => {
      navigation.setOptions({
        headerRight: () => (
          <TouchableOpacity onPress={() => handleUserOk(user)} style={styles.headerButton}>
            <Image source={iconUser} style={styles.headerImage} />
          </TouchableOpacity>
        ),
      });
    }, [navigation, user]);
  
  // Manipula a ação de reservar, definindo o dia selecionado e mostrando o modal
  const handleReserve = (day) => {
    setSelectedDay(day);
    setModalVisible(true);
  };

  // Manipula a confirmação do modal
  const handleModalOk = async () => {
    setModalVisible(false);
    try {
      const selectedDate = parse(selectedDay, 'dd/MM/yyyy', new Date());
      const formattedDate = format(selectedDate, 'yyyy-MM-dd');
      const response = await api.createBooking({
        Date_init: formattedDate,
        Date_end: formattedDate,
        FK_ID_area: area.ID_area,
        FK_email: user.Email,
      });

      // Mostra o alerta de sucesso
      if (response.status === 201) {
        setAlertMessage("Reserva confirmada. Você reservou este espaço para o dia " + selectedDay);
        setSuccessAlertVisible(true);
        setTimeout(() => setSuccessAlertVisible(false), 3000);
      }
    } catch (error) {
      if (error.response && error.response.status === 400) {
        message = error.response.data.error;
      } else if (error.response && error.response.status === 500) {
        message = "Erro ao processar a requisição";
      }
      setAlertMessage(message);
      setAlertVisible(true);
      setTimeout(() => setAlertVisible(false), 3000);
    }
  };

  // Manipula a ação de erro no modal
  const handleModalError = () => {
    setModalVisible(false);
    setAlertMessage("Escolha o dia correto");
    setAlertVisible(true);
    setTimeout(() => setAlertVisible(false), 3000);
  };

  // Renderiza um item da lista de dias
  const renderItem = ({ item }) => (
    <TouchableOpacity style={styles.dayContainer} onPress={() => handleReserve(item)}>
      <Text style={styles.dayText}>{item}</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text style={styles.textTitle}>Escolha o dia do seu aluguel</Text>
      <FlatList
        data={weekDays}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        contentContainerStyle={{ flexGrow: 1, paddingBottom: 20 }}
      />
      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalText}>Você pretende reservar este espaço para o dia {selectedDay}?</Text>
            <View style={styles.modalButtonContainer}>
              <TouchableOpacity style={[styles.modalButton, styles.confirmButton]} onPress={handleModalOk}>
                <Text style={styles.modalButtonText}>AGENDAR</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.modalButton, styles.cancelButton]} onPress={handleModalError}>
                <Text style={styles.modalButtonText}>NÃO, QUERO OUTRO DIA</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {alertVisible && (
        <Animated.View
          entering={SlideInUp.duration(500)}
          exiting={SlideOutDown.duration(500)}
          style={styles.alertContainer}
        >
          <Text style={styles.alertText}>{alertMessage}</Text>
        </Animated.View>
      )}
      {successAlertVisible && (
        <Animated.View
          entering={SlideInUp.duration(500)}
          exiting={SlideOutDown.duration(500)}
          style={styles.alertContainer}
        >
          <Text style={styles.alertText}>Reserva confirmada</Text>
          <Text style={styles.alertSubText}>Você reservou este espaço para o dia {selectedDay}.</Text>
        </Animated.View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#463e31',
  },
  description: {
    color: '#fff',
    fontSize: 16,
    marginBottom: 10,
  },
  dayContainer: {
    backgroundColor: '#E7DDDA',
    marginBottom: 20,
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  dayText: {
    color: 'black',
    fontSize: 16,
    marginBottom: 5,
  },
  textTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    marginBottom: 20,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    width: '80%',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  modalText: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  modalButtonContainer: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  modalButton: {
    padding: 12,
    borderRadius: 5,
    marginVertical: 5,
    width: '80%',
  },
  confirmButton: {
    backgroundColor: '#4CAF50',
  },
  cancelButton: {
    backgroundColor: '#f44336',
  },
  modalButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  alertContainer: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
    padding: 10,
    backgroundColor: '#333',
    borderRadius: 10,
    alignItems: 'center',
  },
  alertText: {
    color: 'white',
    fontSize: 16,
  },
  alertSubText: {
    color: 'white',
    fontSize: 14,
    marginTop: 5,
  },
  headerButton: {
    marginRight: 10,
  },
  headerImage: {
    width: 30,
    height: 30,
  },
});

export default BookingDefault;
