import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import PageInit from "./src/pageInit";
import Login from "./src/login";
import Cadastro from "./src/cadastro";
import BookingDefault from "./src/booking/bookingDefault";
import PerfilUser from "./src/PerfilUser";
import iconUser from "../mobile_3/assets/iconUser.png";
import ProductList from "./src/productList";
import { useNavigation } from '@react-navigation/native';
import { View, Text, Image, TouchableOpacity } from 'react-native';

const Stack = createStackNavigator();


export default function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="PageInit"
        screenOptions={{
          headerStyle: {
            backgroundColor: '#463e31',
          },
          headerTintColor: '#ECEBE9',
          headerTitleStyle: {
            fontWeight: 'heavy',
          },
          headerTitleAlign: 'center',
        }}
      >
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ title: "Faça o login" }}
        />
        <Stack.Screen
          name="ProductList"
          component={ProductList}
          options={{ title: "Áreas Gourmets" }}
        />
        <Stack.Screen
          name="Cadastro"
          component={Cadastro}
          options={{ title: "Cadastre-se"}}
        />
        <Stack.Screen
          name="PageInit"
          component={PageInit}
          options={{ title: "Áreas VIVER BEM!!!"}}
        />
        <Stack.Screen
          name="BookingDefault"
          component={BookingDefault}
          options={{ title: "Reserva de dias" }}
        />

          <Stack.Screen
          name="PerfilUser"
          component={PerfilUser}
          options={{ title: "Informações Pessoais"}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
